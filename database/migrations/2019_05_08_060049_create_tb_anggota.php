<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbAnggota extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Membuat tabel Anggota
        Schema::create('tb_anggota', function (Blueprint $table) {
            $table->bigIncrements('kd_anggota');
            $table->string('no_anggota',50)->default(null)->unique();
            $table->string('nama',50)->default(null);
            $table->string('tempat',50)->default(null);
            $table->date('tgl_lahir',50)->default(null);
            $table->int('jk',50)->default(null);
            $table->string('alamat',50)->default(null);
            $table->string('kota',50)->default(null);
            $table->string('telp',50)->default(null);
            $table->string('email',50)->default(null);
            $table->text('foto')->default(null);
        });

        //Mengisi data pada table anggota
        DB::table('tb_anggota')->insert(
            array(
                'kd_anggota' => '01',
                'no_anggota' => 'A0001',
                'nama' => 'Admin',
                'tempat' => 'Madiun',
                'tgl_lahir' => '2019/01/01',
                'jk' => '1',
                'alamat' => 'Jl. Thamrin no.35',
                'kota' => 'Madiun',
                'telp' => '081335808100',
                'email' => 'mas.indralp@gmail.com'
            )
        );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_anggota');
    }
}
