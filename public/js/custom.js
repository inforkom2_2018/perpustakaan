$(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //onclick gambar buku
    $("#coverbuku").click(function(){
        $("#file").click();
    });

    //Ketika gambar dipilih
    $("#file").change(function(){
        setImage(this,"#coverbuku");
    })

    $('#tabelnya').DataTable()
    $('#tabelnya2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    
})



// Read Image
function setImage(input,target) {

    if (input.files && input.files[0]) {
      var reader = new FileReader();

      // Mengganti src dari object img#coverbuku
      reader.onload = function(e) {
        $(target).attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]);
    }
  }

//Pilih Buku
function add_buku(kodebuku,judul)
{
    var no = $(".buku-item").length+1;
    var content = '<tr class="buku-item '+kodebuku+'">';
    content += '<td>'+no+'</td>';
    content += '<td>'+kodebuku+'</td>';
    content += '<td>'+judul+'</td>';
    content += '<td>';
    content += '<button type="button" class="btn btn-flat btn-xs btn-danger" kode="'+kodebuku+'" onclick="remove_buku(this)">X</button>';
    content += '<input type="hidden" value="'+kodebuku+'" name="kodebuku[]">';
    content += '<input type="hidden" value="'+judul+'" name="judul[]">';
    content += '</td>';
    content += '</tr>';

    $("#lsBuku").append(content);
}

//Hapus Pilih Buku
function remove_buku(obj){
  var cls = $(obj).attr("kode");
  $("."+cls).remove();
}


$( "#frmBuku" ).validate( {
  rules: {
    kode: "required",
    judul: "required"
  },
  messages: {
    kode: "Kode Buku Wajib di isi",
    judul: "Judul Buku Wajib di isi"
  },
  errorElement: "em",
  errorPlacement: function ( error, element ) {
    // Add the `help-block` class to the error element
    error.addClass( "help-block" );

    // Add `has-feedback` class to the parent div.form-group
    // in order to add icons to inputs
    element.parents( ".col-sm-10" ).addClass( "has-feedback" );

    if ( element.prop( "type" ) === "checkbox" ) {
      error.insertAfter( element.parent( "label" ) );
    } else {
      error.insertAfter( element );
    }

    // Add the span element, if doesn't exists, and apply the icon classes to it.
    if ( !element.next( "span" )[ 0 ] ) {
      //$( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
    }
  },
  success: function ( label, element ) {
    // Add the span element, if doesn't exists, and apply the icon classes to it.
    if ( !$( element ).next( "span" )[ 0 ] ) {
      //$( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
    }
  },
  highlight: function ( element, errorClass, validClass ) {
    $( element ).parents( ".col-sm-10" ).addClass( "has-error" ).removeClass( "has-success" );
   // $( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
  },
  unhighlight: function ( element, errorClass, validClass ) {
    $( element ).parents( ".col-sm-10" ).addClass( "has-success" ).removeClass( "has-error" );
   // $( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
  }
} );