<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    if(!Auth::user()){
        return view('auth.login');
    } else {
        return redirect('dashboard');
    }
});

Route::group(['middleware' => ['isAdmin']], function() {

    Route::get('/dashboard',function(){
        return view('template');
    });

    //Buku
    Route::get('/buku/add', 'BukuController@add');
    Route::get('/buku/list', 'BukuController@daftar');
    Route::post('/buku/save', 'BukuController@simpan');
    Route::get('/buku/delete/{kode}', 'BukuController@hapus');
    Route::get('/buku/edit/{kode}', 'BukuController@rubah');


    //Anggota
    Route::get('/anggota/add', 'AnggotaController@add');
    Route::get('/anggota/list', 'AnggotaController@daftar');

    //Transaksi Peminjaman
    Route::post('/trans/peminjaman', 'TransaksiController@pinjam');
    Route::get('/trans/peminjaman', 'TransaksiController@pinjam');
    Route::post('/trans/peminjaman/save', 'TransaksiController@simpan');
    Route::get('/trans/struk/{nopinjam}', 'TransaksiController@cetakstruk');

    //Transaksi Pengembalian
    Route::post('/trans/pengembalian', 'TransaksiController@kembali');
    Route::get('/trans/pengembalian', 'TransaksiController@kembali');
    Route::post('/trans/pengembalian/save', 'TransaksiController@simpan_kembali');

    //Report
    Route::get('/report/buku', 'ReportController@buku');

    //User
    Route::get('/user/add', 'UserController@add');
    Route::post('/user/save', 'UserController@simpan');
});

Route::group(['middleware' => ['isKaryawan']], function() {

    Route::get('/dashboard',function(){
        return view('template');
    });

    //Buku
    Route::get('/buku/add', 'BukuController@add');
    Route::get('/buku/list', 'BukuController@daftar');
    Route::post('/buku/save', 'BukuController@simpan');
    Route::get('/buku/delete/{kode}', 'BukuController@hapus');
    Route::get('/buku/edit/{kode}', 'BukuController@rubah');

    //Report
    Route::get('/report/buku', 'ReportController@buku');

   
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
