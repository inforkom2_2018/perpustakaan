<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{url('dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>
            {{Auth::user()->name}}
            @if(Auth::user()->level==1) 
              {{ " - Admin " }}
            @else
              {{ " - Karyawan " }}
            @endif
            
            </p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="">
          <a href="{{url('/')}}">
            <i class="fa fa-home"></i> <span>Dashboard</span>
          </a>
        </li>


        <li class="treeview @yield('acbuku')">
          <a href="#">
            <i class="fa fa-book"></i> <span>Koleksi Buku</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('buku/add')}}"><i class="fa fa-circle-o"></i> Tambah Data</a></li>
            <li><a href="{{url('buku/list')}}"><i class="fa fa-circle-o"></i> Daftar Buku</a></li>
          </ul>
        </li>
        <li class="treeview @yield('acanggota')">
          <a href="#">
            <i class="fa fa-users"></i> <span>Anggota</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('anggota/add')}}"><i class="fa fa-circle-o"></i> Tambah Anggota</a></li>
            <li><a href="{{url('anggota/list')}}"><i class="fa fa-circle-o"></i> Daftar Anggota</a></li>
          </ul>
        </li>
        <li class="treeview @yield('ackategori')">
          <a href="#">
            <i class="fa fa-book"></i> <span>Kategori</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#" data-toggle="modal" data-target="#modal-info"><i class="fa fa-circle-o"></i> Tambah Kategori</a></li>
            <li><a href="#" data-toggle="modal" data-target="#modal-info"><i class="fa fa-circle-o"></i> Daftar Kategori</a></li>
          </ul>
        </li>
        <li class="treeview @yield('acpenerbit')">
          <a href="#">
            <i class="fa fa-book"></i> <span>Penerbit</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="index.html"><i class="fa fa-circle-o"></i> Tambah Penerbit</a></li>
            <li><a href="index2.html"><i class="fa fa-circle-o"></i> Daftar Penerbit</a></li>
          </ul>
        </li>
        <li class="treeview @yield('acpengarang')">
          <a href="#">
            <i class="fa fa-book"></i> <span>Pengarang</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="index.html"><i class="fa fa-circle-o"></i> Tambah Pengarang</a></li>
            <li><a href="index2.html"><i class="fa fa-circle-o"></i> Daftar Pengarang</a></li>
          </ul>
        </li>
        <li class="treeview @yield('actrans')">
          <a href="#">
            <i class="fa fa-shopping-cart"></i> <span>Transaksi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('trans/peminjaman')}}"><i class="fa fa-circle-o"></i> Transaksi Peminjaman Buku</a></li>
            <li><a href="{{url('trans/pengembalian')}}"><i class="fa fa-circle-o"></i> Transaksi Pengembalian Buku</a></li>
          </ul>
        </li>
        <li class="treeview @yield('acuser')">
          <a href="#">
            <i class="fa fa-user"></i> <span>User</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('user/add')}}"><i class="fa fa-circle-o"></i> Tambah User</a></li>
            <li><a href="index2.html"><i class="fa fa-circle-o"></i> Daftar User</a></li>
          </ul>
        </li>
        <li class="treeview @yield('aclaporan')">
          <a href="#">
            <i class="fa fa-book"></i> <span>Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('report/buku')}}"><i class="fa fa-circle-o"></i> Laporan Data Buku</a></li>
            <li><a href="index2.html"><i class="fa fa-circle-o"></i> Laporan Data Anggota</a></li>
            <li><a href="index2.html"><i class="fa fa-circle-o"></i> Laporan Data Peminjaman</a></li>
            <li><a href="index2.html"><i class="fa fa-circle-o"></i> Laporan Data Pengembalian</a></li>
          </ul>
        </li>

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  <div class="modal modal-info fade" id="modal-info">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Info menu</h4>
      </div>
      <div class="modal-body">
        <p>Buat sama seperti data buku&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-outline">Save changes</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>