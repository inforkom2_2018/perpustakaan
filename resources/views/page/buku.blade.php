@extends('body')
@section('judul')
    Form Buku
@stop
@section('subjudul')
    Control Panel Buku
@stop
@section('bread')
    Form Buku
@stop
@section('acbuku')
    active
@stop
@section('isicontent')
<!-- form start -->
<form id="frmBuku" class="form-horizontal" method="post" action="{{url('buku/save')}}" enctype="multipart/form-data">
@csrf
<div class="row">
    <div class="col-md-3">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Gambar Buku</h3>
            </div>
          
            <div class="box-body">  
                @if($buku->cover=="")
                    <img id="coverbuku" src="{{asset('images/noimage.jpg')}}" alt="" style="width:100%;"> 
                @else
                    <img id="coverbuku" src="{{asset('sampul/'.$buku->cover)}}" alt="" style="width:100%;"> 
                @endif
                <input id="file" type="file" name="gambarbuku" style="display:none">
                <input type="hidden" name="old_foto" value="{{ $buku->cover }}">
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Form Data Buku</h3>
            </div>
            <!-- /.box-header -->
           
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Kode Buku</label>

                        <div class="col-sm-10">
                        <input type="hidden" name="kd_buku" value="{{ $buku->kd_buku }}">
                        <input id="kode" type="text" class="form-control"  placeholder="Kode Buku" name="kode" value="{{ $buku->kd_buku }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Judul Buku</label>

                        <div class="col-sm-10">
                        <input id="judul" type="text" class="form-control"  placeholder="Judul Buku" name="judul" value="{{ $buku->judul }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Pengarang</label>

                        <div class="col-sm-10">
                        <select class="form-control select2" style="width: 100%;" name="pengarang">
                            <option>--Pilih Pengarang--</option>
                            @foreach($pengarang as $rspeng)
                            <option selected="{{ $buku->kd_pengarang==$rspeng->kd_pengarang ? 'selected' :'' }}" value="{{ $rspeng->kd_pengarang}}">{{$rspeng->nama_pengarang }}</option>
                            @endforeach
                        </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Penerbit</label>

                        <div class="col-sm-10">
                        <select class="form-control select2" style="width: 100%;" name="penerbit">
                            <option>--Pilih Penerbit--</option>
                            @foreach($penerbit as $rspenerbit)
                            <option selected="{{ $buku->kd_penerbit==$rspenerbit->kd_penerbit ? 'selected' :'' }}" value="{{ $rspenerbit->kd_penerbit}}">{{$rspenerbit->nama_penerbit }}</option>
                            @endforeach
                        </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tempat / Tahun Terbit</label>

                        <div class="col-sm-5">
                        <input type="text" class="form-control"  placeholder="Tempat terbit" name="tmpt_terbit" value="{{ $buku->tempat_terbit }}">
                        </div>
                        <div class="col-sm-5">
                        <input type="text" class="form-control"  placeholder="Tahun Terbit" name="thn_terbit" value="{{ $buku->tahun_terbit }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Kategori</label>

                        <div class="col-sm-10">
                        <select class="form-control select2" style="width: 100%;" name="kategori">
                            <option>--Pilih Kategori--</option>
                            @foreach($kategori as $rskat)
                            <option selected="{{ $buku->kd_kategori==$rskat->kd_kategori ? 'selected' :'' }}" value="{{ $rskat->kd_kategori}}">{{$rskat->nama_kategori }}</option>
                            @endforeach
                        </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Halaman</label>

                        <div class="col-sm-10">
                        <input type="text" class="form-control"  placeholder="Halaman" name="halaman" value="{{ $buku->halaman }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Edisi</label>

                        <div class="col-sm-10">
                        <input type="text" class="form-control"  placeholder="Edisi" name="edisi" value="{{ $buku->edisi }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ISBN</label>

                        <div class="col-sm-10">
                        <input type="text" class="form-control"  placeholder="ISBN" name="isbn" value="{{ $buku->ISBN }}">
                        </div>
                    </div>
                
                
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                
                <button type="submit" class="btn btn-info pull-right">Simpan Data</button>
                </div>
                <!-- /.box-footer -->
          
        </div>
    
    </div>

</div>
</form>
@stop