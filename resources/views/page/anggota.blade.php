@extends('body')
@section('judul')
    Form Anggota
@stop
@section('subjudul')
    Control Panel Anggota
@stop
@section('bread')
    Form Anggota
@stop
@section('acanggota')
    active
@stop
@section('isicontent')
<!-- form start -->
<form class="form-horizontal" method="post" action="{{url('Anggota/save')}}" enctype="multipart/form-data">
@csrf
<div class="row">
    <div class="col-md-3">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Foto Anggota</h3>
            </div>
          
            <div class="box-body">  
                <img id="coverAnggota" src="{{asset('images/noimage.jpg')}}" alt="" style="width:100%;"> 
                <input id="file" type="file" name="gambarAnggota" style="display:none">
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Form Data Anggota</h3>
            </div>
            <!-- /.box-header -->
           
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">No Anggota</label>

                        <div class="col-sm-10">
                        <input type="text" class="form-control"  placeholder="Nomor Anggota" name="no">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama Anggota</label>

                        <div class="col-sm-10">
                        <input type="text" class="form-control"  placeholder="Nama Anggota" name="nama">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tempat / Tgl Lahir</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control"  placeholder="Tempat Lahir" name="tmpt">
                        </div>
                        <div class="col-sm-5">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="datepicker">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Jenis Kelamin</label>
                        <div class="col-sm-10">
                            <label> 
                                <input type="radio" name="r1" class="minimal" checked> Laki-Laki
                            </label>
                            <label> 
                                <input type="radio" name="r1" class="minimal"> Perempuan
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Alamat</label>

                        <div class="col-sm-10">
                            <textarea class="form-control" rows="3" placeholder="Enter ..."></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Kota</label>

                        <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-map-pin"></i></span>
                            <input type="text" class="form-control" placeholder="Kota" name="kota">
                        </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">No Telp</label>

                        <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                            <input type="text" class="form-control" placeholder="Telephone" name="telp">
                        </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Email</label>

                        <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            <input type="email" class="form-control" placeholder="Email" name="email">
                        </div>
                        </div>
                    </div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                
                <button type="submit" class="btn btn-info pull-right">Simpan Data</button>
                </div>
                <!-- /.box-footer -->
          
        </div>
    
    </div>

</div>
</form>

@stop