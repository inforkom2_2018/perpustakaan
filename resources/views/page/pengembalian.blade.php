@extends('body')
@section('judul')
    Pengembalian Buku
@stop
@section('subjudul')
    Masukkan Nomor Peminjaman Buku dengan benar
@stop
@section('bread')
    Pengembalian Buku
@stop
@section('actrans')
    active
@stop
@section('isicontent')
<div class="row">
    <div class="col-md-12">

        @if($pinjam=="")
        <form id="frmPinjam" action="{{ url('trans/pengembalian') }}" method="post">
            @csrf
            <div class="box box-info">
                <div class="box-body">  
                    <div class="form-group">
                        <label class="col-sm-12" style="text-align:center;">Nomor Peminjaman</label>
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4"><input type="text" class="form-control"  placeholder="Nomor Peminjaman Buku" name="nopinjam" value=""></div>
                        <div class="col-sm-4"></div>
                    </div>
                </div>
            </div>
        </form>
        @else
        <form id="frmPinjam" action="{{ url('trans/pengembalian/save') }}" method="post">
            @csrf
            <div class="box box-info">
            
                <!-- Tampil data peminjam buku -->
                <div class="box-header with-border">
                    <h3 class="box-title">No Pinjam : #{{$pinjam[0]->no_pinjam}}</h3>
                </div>
                <div class="box-body">  
                    <div class="form-group">
                        <input type="hidden" class="form-control" name="nopinjam" value="{{$pinjam[0]->no_pinjam}}">    
                        <strong>
                        ID Anggota : {{ $pinjam[0]->no_anggota }}<br/>
                        Nama       : {{ $pinjam[0]->nama }} </strong> 
                    </div>
                </div>

                <!-- Proses Pinjam -->
                <div class="box-header">
                    <h3 class="box-title">Detail Peminjaman Buku</h3>
                </div>

                 <!-- Table List Buku yang Dipinjam -->
                <table class="table table-hover"  style="margin-top: 15px;">
                    <tbody id="lsBuku">
                        <tr style="background:#ccc;">
                            <th width="15%">Kode Buku</th>
                            <th >Judul</th>
                            <th width="5%">Telat</th>
                            <th width="5%">Denda</th>
                        </tr>
                        @foreach($pinjam as $rsPinjam)
                        <tr>
                            <th width="10%">
                            {{ $rsPinjam->kode_buku }}
                            <input type="hidden" name="kode_buku[]" value="{{ $rsPinjam->kode_buku }}">
                            </th>
                            <th>{{ $rsPinjam->judul }}</th>
                            <th width="8%">
                            <!-- Menhitung Selisih Hari -->
                            @php                      
                                $start = new DateTime($rsPinjam->tgl_kembali);
                                $end = new DateTime();
                                $selisih = $end->diff($start);
                                if($end>$start){
                                    $telat = $selisih->d;
                                } else {
                                    $telat = 0;
                                }
                                echo $telat
                            @endphp
                            </th>
                            <th width="8%">
                            <!-- Denda 2000 Perhari -->
                            {{ $telat * 2000 }}
                            <input type="hidden" name="denda" value="{{ $selisih->d * 2000 }}">
                            </th>
                        </tr>       
                        @endforeach  
                    </tbody>
                </table>

                <!-- Untuk Memasukkan data buku ke table silahkan cek custom.js pada public -->
                <!--- Footer Box -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-success btn-flat">SAVE</button>
                    <a href="{{ url('trans/pengembalian') }}"><button type="button" class="btn btn-warning btn-flat">CANCEL</button></a>
                </div> 
            </div> 
            </div>
        </form>
        @endif
    </div>
</div>


@stop