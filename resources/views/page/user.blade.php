@extends('body')
@section('judul')
    Form User
@stop
@section('subjudul')
    Control Panel User admin
@stop
@section('bread')
    Form User
@stop
@section('acuser')
    active
@stop
@section('isicontent')
<!-- form start -->
<form class="form-horizontal" method="post" action="{{url('user/save')}}" enctype="multipart/form-data">
@csrf
<div class="row">
    
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Form Data User</h3>
            </div>
            <!-- /.box-header -->
           
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama</label>

                        <div class="col-sm-10">
                        <input type="hidden" name="kd_buku" value="">
                        <input type="hidden" name="token" value="{{csrf_token()}}">
                        <input type="text" class="form-control"  placeholder="Nama User" name="nama" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Alamat</label>

                        <div class="col-sm-10">
                        <textarea class="form-control" rows="3" placeholder="Alamat Lengkap" name="alamat"></textarea></div>
                    </div>
                  
                    <div class="form-group">
                        <label class="col-sm-2 control-label">No Telp</label>

                        <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                            <input type="text" class="form-control" placeholder="Telephone" name="telp">
                        </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Email</label>

                        <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            <input type="email" class="form-control" placeholder="Email" name="email">
                        </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Password</label>

                        <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            <input type="password" class="form-control" placeholder="Password" name="pass">
                        </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="level" class="col-sm-2 control-label">Level</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="level" value="">
                                <option value="">- Pilih Level User -</option>
                                <option value="1">Admin</option>
                                <option value="2">Karyawan</option>
                            </select>
                        </div>
                    </div>  
                
                
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                
                <button type="submit" class="btn btn-info pull-right">Simpan Data</button>
                </div>
                <!-- /.box-footer -->
          
        </div>
    
    </div>

</div>
</form>

@stop