@extends('body')
@section('judul')
    Peminjaman Buku
@stop
@section('subjudul')
    Masukkan Transaksi Peminjaman dengan benar
@stop
@section('bread')
    Peminjaman Buku
@stop
@section('actrans')
    active
@stop
@section('isicontent')
<div class="row">
    <div class="col-md-12">
        @if($anggota=="")
        <form id="frmPinjam" action="{{ url('trans/peminjaman') }}" method="post">
            @csrf
            <div class="box box-info">
                <div class="box-body">  
                    <div class="form-group">
                        <label class="col-sm-12" style="text-align:center;">Nomor Anggota</label>
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4"><input type="text" class="form-control"  placeholder="Nomor Anggota Perpustakaan" name="noang" value=""></div>
                        <div class="col-sm-4"></div>
                    </div>
                </div>
            </div>
        </form>
        @if(session('nopinjam'))
        <script>
            window.open("{{url('trans/struk/'.session('nopinjam'))}}", "_blank", "toolbar=0,scrollbars=0,resizable=0,top=0,left=0,width=400,height=600");
       </script>
        @endif
        @else
        <form id="frmPinjam" action="{{ url('trans/peminjaman/save') }}" method="post">
            @csrf
            <div class="box box-info">
            
                <!-- Tampil data peminjam buku -->
                <div class="box-header with-border">
                    <h3 class="box-title">Data Peminjam Buku</h3>
                </div>
                <div class="box-body">  
                    <div class="form-group">
                        <input type="hidden" class="form-control" name="noang" value="{{$anggota->no_anggota}}">    
                        <strong>{{ $anggota->nama }} ( {{ $anggota->no_anggota }} )</strong><br/>
                        {{ $anggota->alamat." ".$anggota->kota }}<br/>
                        Email : {{ $anggota->email }}<br/>
                        Telepon : {{ $anggota->telp }}  
                    </div>
                </div>

                <!-- Proses Pinjam -->
                <div class="box-header">
                    <h3 class="box-title">Detail Peminjaman Buku</h3>
                    <div class="box-tools">
                        <button type="button" class="btn btn-primary btn-flat" data-toggle="modal" data-target="#m-buku">
                        ADD BUKU
                        </button>
                    </div>
                </div>

                 <!-- Table List Buku yang Dipinjam -->
                <table class="table table-hover"  style="margin-top: 15px;">
                    <tbody id="lsBuku">
                        <tr style="background:#ccc;">
                            <th width="2%">#</th>
                            <th width="15%">No Induk Buku</th>
                            <th>Judul</th>
                            <th width="5%">Action</th>
                        </tr>
                    </tbody>
                </table>

                <!-- Untuk Memasukkan data buku ke table silahkan cek custom.js pada public -->
                <!--- Footer Box -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-success btn-flat">SAVE</button>
                    <a href="{{ url('trans/peminjaman') }}"><button type="button" class="btn btn-warning btn-flat">CANCEL</button></a>
                </div> 
            </div> 
            </div>
        </form>
        @endif
    </div>
</div>

<!-- List Data Buku -->
<div class="modal fade" id="m-buku">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Data Buku Perpustakaan</h4>
        </div>
        <div class="modal-body">
            <table id="tabelnya" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th width="2%">#</th>
                        <th width="81%">Judul</th>
                        <th width="5%">Edisi</th>
                        <th width="2%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <!-- Menampilkan Data Anggota -->
                    @foreach($buku as $rsBuku)
                    <tr>
                        <td>{{ $rsBuku->kd_buku }}</td>
                        <td>{{ $rsBuku->judul }}</td>                    
                        <td>{{ $rsBuku->edisi }}</td>
                        <td>
                            <button class="btn btn-primary btn-xs btn-flat" onclick="add_buku('{{ $rsBuku->kd_buku }}','{{ $rsBuku->judul }}')" data-dismiss="modal">PILIH</button>
                             <!-- Untuk Memasukkan data buku ke table silahkan cek custom.js pada public -->
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
       
    </div>
    <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- End Buku-->

@stop