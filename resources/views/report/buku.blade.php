<style>
table { position: relative; border-collapse:collapse; width: 100%; }
table td { border:1px solid #000; padding: 5px; }
h1,h2,p { margin: 0; text-align: center;}
p { padding-bottom: 15px; margin-bottom: 15px; border-bottom: 8px double #000; }
.title { background: #ccc; }
</style>

<h1>PEPRUSTAKAAN NASIONAL</h1>
<h2>REPUBLIK INDONESIA</h2>
<p>Jl Jakarta No.1 Jakarta, Telp : (021) 7777777 
<br>www.perpustakaannasional.com, perpus@perpustakaannasional.com</p>

<table>
    <tr class="title">
        <td width="10%">Kode</td>
        <td width="50%">Judul</td>
        <td>Pengarang</td>
        <td>Penerbit</td>
        <td>Edisi</td>
    </tr>
    @foreach($buku as $rsBuku)
    <tr>
        <td>{{ $rsBuku->kd_buku }}</td>
        <td>{{ $rsBuku->judul }}</td>
        <td>{{ $rsBuku->nama_pengarang }}</td>
        <td>{{ $rsBuku->nama_penerbit }}</td>
        <td>{{ $rsBuku->edisi }}</td>
    </tr>    
    @endforeach
</table>