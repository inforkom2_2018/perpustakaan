@extends('body')
@section('judul')
    List Anggota
@stop
@section('subjudul')
    List Anggota
@stop
@section('bread')
    List Anggota
@stop
@section('acanggota')
    active
@stop
@section('isicontent')
<div class="box">
    <div class="box-header">
        <h3 class="box-title">Daftar Anggota Perpustakaan</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="tabelnya" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>No Anggota</th>
            <th>Nama Anggota</th>
            <th>Alamat</th>
            <th>Telp</th>
            <th>Kategori</th>
            <th>Email</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
      
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
           
            <td>
                <a href=""><button type="button" class="btn bg-green btn-flat"><i class="fa fa-pencil"></i></button></a>
                <a href=""><button type="button" class="btn bg-red btn-flat"><i class="fa fa-trash"></i></button></a>
            </td>
        </tr>

        </tbody>
        <tfoot>
        <tr>
        <th>No Anggota</th>
            <th>Nama Anggota</th>
            <th>Alamat</th>
            <th>Telp</th>
            <th>Kategori</th>
            <th>Email</th>
            <th>Action</th>
        </tr>
        </tfoot>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->

@stop