@extends('body')
@section('judul')
    List Buku
@stop
@section('subjudul')
    List Buku
@stop
@section('bread')
    List Buku
@stop
@section('acbuku')
    active
@stop
@section('isicontent')
<div class="box">
    <div class="box-header">
        <h3 class="box-title">Daftar Buku Perpustakaan</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="tabelnya" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Kode Buku</th>
            <th>Judul Buku</th>
            <th>Pengarang</th>
            <th>Penerbit</th>
            <th>Kategori</th>
            <th>Edisi</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $rsbuku)
        <tr>
            <td>{{ $rsbuku->kd_buku }}</td>
            <td>{{ $rsbuku->judul }}</td>
            <td>{{ $rsbuku->nama_pengarang }}</td>
            <td>{{ $rsbuku->nama_penerbit }}</td>
            <td>{{ $rsbuku->nama_kategori }}</td>
            <td>{{ $rsbuku->edisi }}</td>
            <td>
                <a href="{{url('buku/edit/'.$rsbuku->kd_buku)}}"><button type="button" class="btn bg-yellow btn-flat"><i class="fa fa-pencil"></i></button></a>
                <a href="{{url('buku/delete/'.$rsbuku->kd_buku)}}"><button type="button" class="btn bg-red btn-flat"><i class="fa fa-trash"></i></button></a>
            </td>
        </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <th>Kode Buku</th>
            <th>Judul Buku</th>
            <th>Pengarang</th>
            <th>Penerbit</th>
            <th>Kategori</th>
            <th>Edisi</th>
            <th>Action</th>
        </tr>
        </tfoot>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->


@stop