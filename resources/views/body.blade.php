@include('header')
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  @include('main_head')

  @include('sidebar_menu')

  @include('content')

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
@include('file_body')

<!-- /.modal -->
</body>
</html>