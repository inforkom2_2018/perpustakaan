<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BukuController extends Controller
{
    //
    public function add()
    {
        //Ketika tombol tambah data di klik
        $buku=(Object)["kd_buku"=>"","judul"=>"","kd_pengarang"=>"","kd_penerbit"=>"","tempat_terbit"=>"","tahun_terbit"=>"","kd_kategori"=>"","halaman"=>"","edisi"=>"","ISBN"=>"","cover"=>""];
        $pengarang = DB::table('tb_pengarang')->get();
        $penerbit = DB::table('tb_penerbit')->get();
        $kategori = DB::table('tb_kategori')->get();
        return view ('page/buku',compact('pengarang','penerbit','kategori','buku'));
    }

    public function daftar()
    {
        //Unutk menampilkan data di dalam table yang sudah dibuat
        $data = DB::select('SELECT a.kd_buku, a.judul, b.nama_pengarang, c.nama_penerbit, d.nama_kategori, a.edisi FROM tb_buku a
        INNER JOIN tb_pengarang b ON a.kd_pengarang=b.kd_pengarang
        INNER JOIN tb_penerbit c ON a.kd_penerbit=c.kd_penerbit
        INNER JOIN tb_kategori d ON a.kd_kategori=d.kd_kategori');
        return view ('data.listbuku',compact('data'));
    }

    public function simpan(Request $req)
    {
        //Cek nama file foto
        if($req->file('gambarbuku')){
            $foto = $req->file('gambarbuku');
            $nama_foto = date('m-Y-').$foto->getClientOriginalName();
        } else {
            $nama_foto = $req->get('old_foto');
        }

        //Simpan table buku
        $kd = $req->kode;
        $judul = $req->judul;
        $kdpeng = $req->pengarang;
        $kdpener = $req->penerbit;
        $tmpt = $req->tmpt_terbit;
        $thn = $req->thn_terbit;
        $kdkat = $req->kategori;
        $hal = $req->halaman;
        $edisi = $req->edisi;
        $isbn = $req->isbn;


        if($req->get('kd_buku')=="")
        {
            //Simpan add
            DB::table('tb_buku')->insert([
                "kd_buku" => $kd,
                "judul" => $judul,
                "kd_pengarang" => $kdpeng,
                "kd_penerbit" => $kdpener,
                "tempat_terbit" => $tmpt,
                "tahun_terbit" => $thn,
                "kd_kategori" => $kdkat,
                "halaman" => $hal,
                "edisi" => $edisi,
                "ISBN" => $isbn,
                "cover" => $nama_foto
            ]);
        
        }else{

            //simpan edit
            DB::table('tb_buku')->where("kd_buku",$kd)->update([
                "judul" => $judul,
                "kd_pengarang" => $kdpeng,
                "kd_penerbit" => $kdpener,
                "tempat_terbit" => $tmpt,
                "tahun_terbit" => $thn,
                "kd_kategori" => $kdkat,
                "halaman" => $hal,
                "edisi" => $edisi,
                "ISBN" => $isbn,
                "cover" => $nama_foto
            ]);
       
        }

        //Memindah File gambar
        if($req->file('gambarbuku')){
            $foto->move(public_path()."/sampul", $nama_foto);
        }

        return redirect('buku/list');


    }

    public function hapus($kode)
    {
        //Ketika tombol hapus data di klik
        DB::table('tb_buku')->where("kd_buku",$kode)->delete();
        return redirect('buku/list');
    }

    public function rubah($kode)
    {
        //ketika tombol edit di klik untuk menampilkan data pada form seusai kode buku
        $pengarang = DB::table('tb_pengarang')->get();
        $penerbit = DB::table('tb_penerbit')->get();
        $kategori = DB::table('tb_kategori')->get();
        $buku = DB::table('tb_buku')->where("kd_buku",$kode)->first();

        return view ('page.buku',compact('pengarang','penerbit','kategori','buku'));
    }
}
