<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index($namanya=null){
        return view('Contact',compact("namanya"));
    }

    public function proses(Request $req){
        $nama=$req->nama;
        $email=$req['email'];
        $keterangan=$req['ket'];
        return "Nama anda $nama dan email anda $email. Pesan anda $keterangan";
    }
}

