<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransaksiController extends Controller
{

    //================ TRANSAKSI PEMINJAMAN BUKU ================================
    public function pinjam(Request $req)
    {
        if($req){
            $buku=DB::table('tb_buku')->where('status',"Ada")->get();
            $anggota=DB::table('tb_anggota')->where('no_anggota',$req->noang)->first();
        }else{
            $anggota="";
            $buku="";
        }
        return view ('page/peminjaman',compact('anggota','buku'));

    }

    //Simpan transaksi peminjaman
    public function simpan(Request $req)
    {
        //Membuat kode peminjaman otomatis
        $jmlrec = DB::table('tb_peminjaman')->orderBy('kd_pinjam','desc')->first();

        if($jmlrec!=null)
        {
            $nopinjam = "P".sprintf('%06d',$jmlrec->kd_pinjam);      
        }else{
            $nopinjam = "P000001";   
        }
        foreach($req->get('kodebuku') as $kode){
            //Simpan ke tabel peminjaman
            DB::table('tb_peminjaman')->insert([
                'no_pinjam'=>$nopinjam,
                'tgl_pinjam'=>date('Y-m-d'),
                'tgl_kembali'=>date('Y-m-d',strtotime('+3 days')),
                'kode_buku'=>$kode,
                'no_anggota'=>$req->get('noang')
            ]);

            //Update status buku menjadi terpinjam
            DB::table('tb_buku')->where("kd_buku",$kode)->update([
                'status'=>'Terpinjam'
            ]);

            //Cetak struk peminjaman


        }

       return redirect('trans/peminjaman')->with('nopinjam',$nopinjam);
    }

    public function cetakstruk($nopinjam)
    {
        $data = DB::select("SELECT a.no_pinjam, a.kode_buku, b.judul, a.no_anggota, c.nama, a.tgl_kembali, a.tgl_pinjam FROM tb_peminjaman a
        INNER JOIN tb_buku b ON a.kode_buku=b.kd_buku
        INNER JOIN tb_anggota c ON a.no_anggota = c.no_anggota where a.no_pinjam='".$nopinjam."'");

        return view('page/struk',compact('data'));
    }
    //============END PEMINJAMAN BUKU =======================================================
  
    //============TRANSAKSI PENGEMBALIAN BUKU ================================================

    public function kembali(Request $req)
    {
        if(count($req->all())>0){
            $pinjam = DB::select("SELECT a.no_pinjam, a.kode_buku, b.judul, a.no_anggota, c.nama, a.tgl_kembali FROM tb_peminjaman a
            INNER JOIN tb_buku b ON a.kode_buku=b.kd_buku
            INNER JOIN tb_anggota c ON a.no_anggota = c.no_anggota WHERE a.status=0 and a.no_pinjam='".$req->get('nopinjam')."'");

             //Jika data tidak ditemukan
            if(count($pinjam)==0){
                $pinjam="";
            }

        }else{
            $pinjam="";
        }

       
        return view('page/pengembalian',compact('pinjam'));
    }

    public function simpan_kembali(Request $req)
    {
        foreach($req->get('kode_buku') as $kode){    
            // Simpan ke tabel peminjaman      
            DB::table('tb_peminjaman')->where('no_pinjam',$req->get('nopinjam'))->where('kode_buku',$kode)->update([
                'denda'=>$req->get('denda'),
                'status'=>1
            ]);
            
            // Update status buku jadi terpinjam
            DB::table('tb_buku')->where("kd_buku",$kode)->update([
                'status'=>'Ada'
            ]);
        }

        return redirect('trans/pengembalian');
    }
   
}
