<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    //
    public function add()
    {
        //Ketika tombol tambah data di klik
        return view ('page/user');
    }

    public function simpan(Request $req)
    {
    
        //Simpan add
        DB::table('users')->insert([
            "name" => $req->nama,
            "alamat" => $req->alamat,
            "telp" => $req->telp,
            "email" => $req->email,
            "password" => Hash::make($req->pass),
            "remember_token" => $req->token,
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s'),
            "level" => $req->level
        ]);
        return redirect('user/add');

    }
}
